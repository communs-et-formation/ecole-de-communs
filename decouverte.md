# Module découverte

Autour d'un scénario, les participants / joueurs évaluent des situations, dialoguent et échangent autour de la gestion collective et de la propriété. Chaque étape est l'occasion de découvrir un pan de ce qui constitue l'architecture des communs, ses implications et ses alternatives.

## Introduction

1. Un des participants récupère gratuitement un vélo encore en bon état
2. Il s'en sert de temps en temps mais pas très souvent
3. Un ami qui n'a pas de vélo lui emprunte
4. [Case Départ] Le joueur se dit qu'il pourrait proposer à d'autres amis d'utiliser ce vélo dont il ne se sert pas trop
5. Le vélo est utilisé régulièrement par ce groupe d'ami
> Point étape : La mutualisaton d'une ressource (qui n'est pas encore un commun...)

## De la ressource au commun
6. Au fur et à mesure qu'il est utilisé, l'organisation pour accéder au vélo se complexifie...
7. ... et cette organisation pèse surtout sur le détenteur du vélo.
8. Trois options : 
    - Le détenteur du vélo s'énerve et ne prête plus le vélo, après tout c'est lui qui l'a trouvé
        - Après quelques temps, il revient à la [Case départ]
    - Le détenteur du vélo fixe ses propres règles pour accéder au vélo, après tout c'est lui qui l'a trouvé
        - Les règles restrictives découragent les autres utilisateurs et il revient à la [Case départ]
    - Le détenteur du vélo revend le vélo, après tout c'est lui qui l'a trouvé
        - Quelques mois après, il a besoin d'un vélo et doit un acheter un qui lui coûte cher et dont il se sert peu, retour à la [Case départ]
> Point étape : Question de la propriété (à qui appartient réellement ce vélo : propriétaire vs usagers)


## Gérer un commun : l'usage
9. Le groupe décide d'une propriété collective du vélo...
10. ... et pour ne pas faire peser l'organisation sur une personne, décide de faire tourner cette gestion entre eux 
11. Après quelques temps, certains utilisateurs quitte le groupe, d'autres le rejoignent
12. L'entrée dans le groupe se fait sur la confiance, les connaissances communes, une cooptation...
> Point étape : La gouvernance 1/2 (Ouvrir la ressource, libérer l'usage)
13. Le vélo est bien utilisé, mais la gestion se compléxifie encore avec les nouveaux entrants qui n'ont pas l'historique du projet
14. Cela commence a généré de nouveaux problèmes : le vélo n'est pas rendu à temps, pas au bon endroit...
15. Le groupe "des anciens" décident de mettre en place des règles plus strictes (qui peut rentrer dans le groupe, limite d'usage...)
16. On retrouve à peu près les 3 options du 8. mais à l'échelle d'un groupe
> Point étape : La gouvernance 2/2 (Décider ensemble, préserver la ressource et documenter)

## Gérer un commun : l'économie du commun
17. Les règles se stabilisent, elles sont revues régulièrement et s'adaptent à l'usage et la taille du groupe 
18. Occasionnellement, des petits problèmes apparaissent avec le vélo (crevaison, petites réparations...)
19. En général, elles sont prises en charge par un des usagers, mais le vélo demande maitenant quelques réparations plus importantes (et coûteuses)
20. Quelques usagers proposent une réunion pour décider de comment financer ces réparations
21. Lors de cette réunion, certains proposent l'achat d'un vélo neuf plutôt que de réparer. les avis divergent sur le principe, et de plus tous les utilisateurs n'ont pas les mêmes moyens... D'autant que les durée d'utilisation sont très variables
> Point étape : Faire vivre un commun 1/2 

## Gérer un commun :  La contribution
22. Un compromis est trouvé, même si certains usagers ont préféré quitter le groupe. Une très faible cotisation annuelle est demandée, et les usagers complétent le budget avec des dons libres en fonction de leurs usages
23. La caisse commune permet de couvrir les frais de réparation du vélo
24. Toutefois, certains usagers (souvent les mêmes...) se retrouvent en charge de tâches récurrentes :
    - collecter les cotisations et distribuer l'argent en cas de besoin
    - gérer certains tensions quant aux montants des dons libres
    - faire appel à un réparateur spécialisé quand personne n'a les compétences ou le temps pour réparer...
> Point étape : la contribution

Suite possible : La mise en place de budget contributif, le statut du groupe et des usagers, la création d'une asso, d'une société, la fédération avec d'autres groupes, etc...
