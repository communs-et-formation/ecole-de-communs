# ecole-de-communs

## Proposer des modules de formations autour des communs

Créer une offre de formation abordant sous la forme de modules courts les concepts autour des communs sous une forme pratique.
Formation en CC, avec versionning et "release" : voir etsijaccompagnais.fr

## Modules "coeur"

### [Découverte](decouverte.md) 
*1/2 journée*

Module narratif qui permet de voir à partir d'un scenario comment se construit une logique de communs autour d'une ressource

### Contribuer ?
*1/2 journée*

Les collectifs actifs dans les communs utilisent la notion de contribution. Qu'est-ce que cela signifie ? Où se situe-t-elle dans le champ du travail ? Quels changements de posture et d'organisation cela implique-t-il ?

### Focus : le budget contributif
*1/2 journée + 1/2 journée si mise en oeuvre sur Loot*

Comprendre ce qu'est (et n'est pas) un budget contributif. Mettre en place un budget et l'expérimenter grâce à Loot.

### Animer et prendre soin d'une communauté contributive
*1/2 journée*

Oeuvrer autour des communs c'est prendre soin : d'une ressource, de soi, des autres contributeurs, de l'espace social définit par ce commun. Quelle attention particulière faut-il avoir ? Quelle forme prend cette attention.

### Intégrer les communs dans l'entreprise
*1/2 journée + 1/2 journée sur mesure*

La logique de communs est-elle compatible avec les exigences d'une entreprise ? Qu'est-il possible de mettre en place ?



## Modules complémentaires techniques

### Gouvernance, animation de réunion, prise de parole... (avec entrepreneur optéos ?)

### Comptabilité, social, fiscal, etc... autour des communs (avec Harmonium ?)
